<?php

/**
 * @param $post_id -- id of a saving post ( type: integer )
 * @return youtube link ( type: string )
 */
function find_youtube_video_url( $post_id ) {
	$post_content = get_post( $post_id ); 

	$pattern = '#(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})#x';
	preg_match($pattern, $post_content->post_content, $match);
	
	return $match;
}

/**
 * @param $id -- id of YouTube video ( type: string )
 * @return tag img with thumbnail link ( type: string )
 */
function get_yt_video_thumbnail_by_id( $id ){
	return 'http://img.youtube.com/vi/'. $id .'/mqdefault.jpg';
}

/**
 * @param $url -- YouTube video url ( type: string )
 * @return id of a YouTube video ( type: string )
 */
function get_yt_video_id_by_url( $url ) {
	parse_str(parse_url($url,PHP_URL_QUERY),$arr);
	
	return $arr['v']; 
}

/**
 * @param $url -- YouTube video url ( type: string )
 * @return YouTube video duration ( type: array )
 */
function get_yt_video_duration_by_url( $url ){
		$video_id = get_yt_video_id_by_url($url);
    $data=file_get_contents('https://www.googleapis.com/youtube/v3/videos?key=AIzaSyC2Gn41qH-TLZlT0vi8Akx3wsBwSY21mTU&part=contentDetails&id='. $video_id);
    if (false===$data) 
    	return false;
    $obj=json_decode($data);
    $pattern = '#PT(\d+H)?(\d+M)?(\d+S)#x';
    preg_match($pattern, $obj->items[0]->contentDetails->duration, $match);

    return $match;
}

/**
 * @param $match ( type: string )
 * @return converted YouTube duration to seconds ( type: integer )
 */
function yt_duration_to_seconds( $match ) {
	$hours = (intval($match[1]) || 0);
	$minutes = (intval($match[2]) || 0);
	$seconds = (intval($match[3]) || 0);

	return $hours * 3600 + $minutes * 60 + $seconds;
}

function add_meta_to_head() {
	global $post;
	$meta = '';

	$yt_duration = get_post_meta($post->ID, 'yt_video_duration', true);
	$yt_thumb = get_post_meta($post->ID, 'yt_video_thumbnail', true);

	if ( $yt_duration && $yt_thumb ) {
		$meta .= '<meta property="og:image" content="'. $yt_thumb .'">';
		$meta .= '<meta property="og:video:duration" content="'. $yt_duration .'">';

		echo $meta;
	}
}