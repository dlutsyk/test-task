<?php
/*
Plugin Name: og-youtube-video
Plugin URI: http://og-youtube-video.com/
Description: Test task for gentechpartners.com
Version: 1.0
Author: Dmitriy Lutsyk
Author URI: http://vk.com/keeel
License: GPL
*/

require_once(__DIR__.'/includes/functions.php');

///////////////////////////////////////////////////
/// Main function of the plugin ( start point ) ///
///////////////////////////////////////////////////

function yt_runner( $post_id ) {

	$yt_video_link = ''; // contains YT video link whitch was found in the post
	$yt_video_duration = ''; // contains video duration of the founded video in the post in array
	$yt_video_duration_in_seconds = '';
	$yt_video_thumbnail = ''; // contains YT video thumbnail

	$yt_video_link = find_youtube_video_url( $post_id );

	if ( $yt_video_link ) {

		$yt_video_duration = get_yt_video_duration_by_url( $yt_video_link[0] );
		$yt_video_duration_in_seconds = yt_duration_to_seconds( $yt_video_duration );
		
		$yt_video_thumbnail = get_yt_video_thumbnail_by_id( $yt_video_link[1] );

		update_post_meta( $post_id, 'yt_video_thumbnail', $yt_video_thumbnail );
		update_post_meta( $post_id, 'yt_video_duration', $yt_video_duration_in_seconds );

	}
}

/////////////////////////
//////// Actions ////////
/////////////////////////

add_action( 'save_post', 'yt_runner' );
add_action( 'wp_head', 'add_meta_to_head' );

?>